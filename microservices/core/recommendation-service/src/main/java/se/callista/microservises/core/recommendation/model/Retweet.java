package se.callista.microservises.core.recommendation.model;

/**
 * Created by magnus on 04/03/15.
 */
public class Retweet {
    private int tweetId;
    private int retweetId;
    private String author;
    private int cant;
    private String content;

    public Retweet() {
    }

    public Retweet(int tweetId, int retweetId, String author, int cant, String content) {
        this.tweetId = tweetId;
        this.retweetId = retweetId;
        this.author = author;
        this.cant= cant;
        this.content = content;
    }

    public int getTweetId() {
        return tweetId;
    }

    public void setTweetId(int tweetId) {
        this.tweetId = tweetId;
    }

    public int getRetweetId() {
        return retweetId;
    }

    public void setRetweetId(int retweetId) {
        this.retweetId = retweetId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCant() {
        return cant;
    }

    public void setRate(int cant) {
        this.cant = cant;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
