package se.callista.microservises.core.product.service;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.callista.microservises.core.product.model.Product;

/**
 * Created by magnus on 04/03/15.
 */
@RestController
public class TweetService {

    /**
     * Sample usage: curl $HOST:$PORT/product/1
     *
     * @param productId
     * @return
     */
    @RequestMapping("/tweet/{tweetId}")
    public Tweet getTweet(@PathVariable int tweetId) {

        return new Tweet(tweetId, "tweet1");
    }
}
