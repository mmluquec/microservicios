package se.callista.microservises.core.product.model;

/**
 * Created by magnus on 04/03/15.
 */
public class Tweet {
    private int tweetId;
    private String comment;
    

    public Tweet() {

    }

    public Tweet(int tweetId, String comment) {
        this.tweetId = tweetId;
        this.comment = comment;
        
    }

    public int getTweetId() {
        return tweetId;
    }

    public void setTweetId(int tweetId) {
        this.tweetId = tweetId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
