package se.callista.microservises.core.review.model;

/**
 * Created by magnus on 04/03/15.
 */
public class ListTweet {
    private int tweetId;
    private int listId;
    private String author;
    private String content;

    public ListTweet() {
    }

    public ListTweet(int tweetId, int listId, String author,  String content) {
        this.tweetId = tweetId;
        this.listId = listId;
        this.author = author;
        this.content = content;
    }

    public int getTweetId() {
        return tweetId;
    }

    public void setProductId(int tweetId) {
        this.tweetId = tweetId;
    }

    public int getListId() {
        return listId;
    }

    public void setReviewId(int listId) {
        this.listId = listId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
