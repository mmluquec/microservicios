package se.callista.microservices.composite.product.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import se.callista.microservises.core.product.model.Product;
import se.callista.microservises.core.recommendation.model.Recommendation;
import se.callista.microservises.core.review.model.Review;

import java.io.IOException;
import java.net.URI;
import java.util.List;

/**
 * Created by magnus on 05/03/15.
 */
@Component
public class TweetCompositeIntegration {

    private static final Logger LOG = LoggerFactory.getLogger(TweetCompositeIntegration.class);

    @Autowired
    Util util;

    private RestTemplate restTemplate = new RestTemplate();

    // -------- //
    // TWEET //
    // -------- //

    @HystrixCommand(fallbackMethod = "defaultTweet")
    public ResponseEntity<Tweet> getTweet(int tweetId) {

        URI uri = util.getServiceUrl("tweet", "http://localhost:8081/tweet");
        String url = uri.toString() + "/tweet/" + tweetId;
        LOG.debug("GetTweet from URL: {}", url);

        ResponseEntity<String> resultStr = restTemplate.getForEntity(url, String.class);
        LOG.debug("GetTweet http-status: {}", resultStr.getStatusCode());
        LOG.debug("GetTweet body: {}", resultStr.getBody());

        Tweet tweet = response2Tweet(resultStr);
        LOG.debug("GetTweet.id: {}", tweet.getTweetId());

        return util.createOkResponse(tweet);
    }

    /**
     * Fallback method for getProduct()
     *
     * @param productId
     * @return
     */
    public ResponseEntity<Tweet> defaultTweet(int tweetId) {
        LOG.warn("Using fallback method for tweet-service");
        return util.createResponse(null, HttpStatus.BAD_GATEWAY);
    }

    // --------------- //
    // RETWEET //
    // --------------- //

    @HystrixCommand(fallbackMethod = "defaultRetweet")
    public ResponseEntity<List<Retweet>> getRetweet(int retweetId) {
        try {
            LOG.info("GetRetweet...");

            URI uri = util.getServiceUrl("retweet", "http://localhost:8081/retweet");

            String url = uri.toString() + "/retweet?retweetId=" + retweetId;
            LOG.debug("GetRecommendations from URL: {}", url);

            ResponseEntity<String> resultStr = restTemplate.getForEntity(url, String.class);
            LOG.debug("GetRetweet http-status: {}", resultStr.getStatusCode());
            LOG.debug("GetRetweet body: {}", resultStr.getBody());

            List<Retweet> retweet = response2Retweet(resultStr);
            LOG.debug("GetRetweet.cnt {}", retweet.size());

            return util.createOkResponse(retweet);
        } catch (Throwable t) {
            LOG.error("getRetweet error", t);
            throw t;
//            throw new RuntimeException(t);
        }
    }


    /**
     * Fallback method for getRecommendations()
     *
     * @param productId
     * @return
     */
    public ResponseEntity<List<Retweet>> defaultRetweet(int tweetId) {
        LOG.warn("Using fallback method for retweet-service");
        return util.createResponse(null, HttpStatus.BAD_GATEWAY);
    }


    // ------- //
    // ListTweet //
    // ------- //

    @HystrixCommand(fallbackMethod = "defaultListTweet")
    public ResponseEntity<List<ListTweet>> getListTweet(int tweetId) {
        LOG.info("GetListTweet...");

        URI uri = util.getServiceUrl("listtweet", "http://localhost:8081/listtweet");

        String url = uri.toString() + "/listtweet?tweetId=" + tweetId;
        LOG.debug("GetListTweet from URL: {}", url);

        ResponseEntity<String> resultStr = restTemplate.getForEntity(url, String.class);
        LOG.debug("GetListTweet http-status: {}", resultStr.getStatusCode());
        LOG.debug("GetListTweet body: {}", resultStr.getBody());

        List<Review> listtweet  = response2ListTweet(resultStr);
        LOG.debug("GetReviews.cnt {}", listtweet.size());

        return util.createOkResponse(listtweet);
    }


    /**
     * Fallback method for getReviews()
     *
     * @param productId
     * @return
     */
    public ResponseEntity<List<ListTweet>> defaultReviews(int tweetId) {
        LOG.warn("Using fallback method for listtweet-service");
        return util.createResponse(null, HttpStatus.BAD_GATEWAY);
    }

    // ----- //
    // UTILS //
    // ----- //

    /*
     * TODO: Extract to a common util-lib
     */

    private ObjectReader tweetReader = null;
    private ObjectReader getTweetReader() {

        if (tweetReader != null) return tweetReader;

        ObjectMapper mapper = new ObjectMapper();
        return tweetReader = mapper.reader(Tweet.class);
    }

    private ObjectReader retweetReader = null;
    private ObjectReader getRetweetReader() {
        if (retweetReader != null) return retweetReader;

        ObjectMapper mapper = new ObjectMapper();
        return retweetReader = mapper.reader(new TypeReference<List<Retweet>>() {});
    }

    public Product response2Tweet(ResponseEntity<String> response) {
        try {
            return getTweetReader().readValue(response.getBody());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // TODO: Gereralize with <T> method, skip objectReader objects!
    private List<Retweet> response2Retweet(ResponseEntity<String> response) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List list = mapper.readValue(response.getBody(), new TypeReference<List<Retweet>>() {});
            List<Retweet> retweet = list;
            return retweet;

        } catch (IOException e) {
            LOG.warn("IO-err. Failed to read JSON", e);
            throw new RuntimeException(e);

        } catch (RuntimeException re) {
            LOG.warn("RTE-err. Failed to read JSON", re);
            throw re;
        }
    }

    private List<ListTweet> response2ListTweet(ResponseEntity<String> response) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List list = mapper.readValue(response.getBody(), new TypeReference<List<ListTweet>>() {});
            List<ListTweet> listtweet = list;
            return listtweet;

        } catch (IOException e) {
            LOG.warn("IO-err. Failed to read JSON", e);
            throw new RuntimeException(e);

        } catch (RuntimeException re) {
            LOG.warn("RTE-err. Failed to read JSON", re);
            throw re;
        }
    }

// FIXME: DOESN'T WORK. GIVER ERORS LIKE: Caused by: java.lang.ClassCastException: java.util.LinkedHashMap cannot be cast to se.callista.microservises.core.recommendation.model.Recommendation
//    private <T> T responseString2Type(ResponseEntity<String> response) {
//        try {
//            ObjectMapper mapper = new ObjectMapper();
//            T object = mapper.readValue(response.getBody(), new TypeReference<T>() {});
//            return object;
//
//        } catch (IOException e) {
//            LOG.warn("IO-err. Failed to read JSON", e);
//            throw new RuntimeException(e);
//
//        } catch (RuntimeException re) {
//            LOG.warn("RTE-err. Failed to read JSON", re);
//            throw re;
//        }
//    }
//
//    /**
//     * TODO: DO WE REALLY NEED THIS ONE???
//     *
//     * @param response
//     * @param <T>
//     * @return
//     */
//    private <T> List<T> responseString2List(ResponseEntity<String> response) {
//        try {
//            ObjectMapper mapper = new ObjectMapper();
//            List<T> list = mapper.readValue(response.getBody(), new TypeReference<List<T>>() {});
//            return list;
//
//        } catch (IOException e) {
//            LOG.warn("IO-err. Failed to read JSON", e);
//            throw new RuntimeException(e);
//
//        } catch (RuntimeException re) {
//            LOG.warn("RTE-err. Failed to read JSON", re);
//            throw re;
//        }
//    }
//

}