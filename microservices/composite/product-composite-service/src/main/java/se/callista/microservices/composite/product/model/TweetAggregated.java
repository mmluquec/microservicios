package se.callista.microservices.composite.product.model;

import se.callista.microservises.core.product.model.Product;
import se.callista.microservises.core.recommendation.model.Recommendation;
import se.callista.microservises.core.review.model.Review;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by magnus on 04/03/15.
 */
public class TweetAggregated {
    private int tweetId;
    private String comment;
    private List<RetweetSummary> retweet;
    private List<ListTweetSummary> listtweet;

    public TweetAggregated(Tweet tweet, List<Retweet> retweet, List<ListTweet> listtweet) {

        // 1. Setup product info
        this.tweetId = tweet.getTweetId();
        this.comment = tweet.getComment();
       

        // 2. Copy summary recommendation info, if available
        if (retweet != null)
            this.retweet = retweet.stream()
                .map(r -> new RetweetSummary(r.getRetweetId(), r.getAuthor() ))
                .collect(Collectors.toList());

        // 3. Copy summary review info, if available
        if (listtweet != null)
            this.listtweet = listtweet.stream()
                .map(r -> new ListTweetSummary(r.getListTweetId(), r.getAuthor() ))
                .collect(Collectors.toList());
    }

    public int getTweetId() {
        return tweetId;
    }

    public String getComment() {
        return comment;
    }

    public List<RetweetSummary> getRetweet() {
        return retweet;
    }

    public List<ListTweetSummary> getListTweet() {
        return listtweet;
    }
}
