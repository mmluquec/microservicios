package se.callista.microservices.composite.product.model;

/**
 * Created by magnus on 05/03/15.
 */
public class RetweetSummary {

    private int retweetId;
    private String author;
    

    public RetweetSummary(int retweetId, String author) {
        this.retweetId = retweetId;
        this.author = author;

    }

    public int getRetweetId() {
        return retweetId;
    }

    public String getAuthor() {
        return author;
    }

 
}
