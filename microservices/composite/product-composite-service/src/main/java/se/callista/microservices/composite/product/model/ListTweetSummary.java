package se.callista.microservices.composite.product.model;

/**
 * Created by magnus on 05/03/15.
 */
public class ListTweetSummary {

    private int listtweetId;
    private String author;
    

    public ReviewSummary(int listtweetId, String author) {
        this.listtweetId = listtweetId;
        this.author = author;
    
    }

    public int getListTweetId() {
        return listtweetId;
    }

    public String getAuthor() {
        return author;
    }


}
