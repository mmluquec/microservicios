package se.callista.microservices.composite.product.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import se.callista.microservices.composite.product.model.ProductAggregated;
import se.callista.microservises.core.product.model.Product;
import se.callista.microservises.core.recommendation.model.Recommendation;
import se.callista.microservises.core.review.model.Review;

import java.util.Date;
import java.util.List;

/**
 * Created by magnus on 04/03/15.
 */
@RestController
public class TweetCompositeService {

    private static final Logger LOG = LoggerFactory.getLogger(TweetCompositeService.class);

    @Autowired
    ProductCompositeIntegration integration;

    @Autowired
    Util util;

    @RequestMapping("/")
    public String getTweet() {
        return "{\"timestamp\":\"" + new Date() + "\",\"content\":\"Hello from TweetAPi\"}";
    }

    @RequestMapping("/tweet/{tweetId}")
    public ResponseEntity<TweetAggregated> getTweet(@PathVariable int tweetId) {

        // 1. First get mandatory product information
        ResponseEntity<Tweet> tweetResult = integration.getTweet(tweetId);

        if (!productResult.getStatusCode().is2xxSuccessful()) {
            // We can't proceed, return whatever fault we got from the getProduct call
            return util.createResponse(null, tweetResult.getStatusCode());
        }

        // 2. Get optional recommendations
        List<Retweet> retweet = null;
        try {
            ResponseEntity<List<Retweet>> retweetResult = integration.getRetweet(tweetId);
            if (!retweetResult.getStatusCode().is2xxSuccessful()) {
                // Something went wrong with getRecommendations, simply skip the recommendation-information in the response
                LOG.debug("Call to getRetweet failed: {}", retweetResult.getStatusCode());
            } else {
                retweet = retweetResult.getBody();
            }
        } catch (Throwable t) {
            LOG.error("getTweet erro ", t);
            throw t;
        }


        // 3. Get optional reviews
        ResponseEntity<List<ListTweet>> listTweetResult = integration.getListTweet(tweetId);
        List<ListTweet> listTweet = null;
        if (!listTweetResult.getStatusCode().is2xxSuccessful()) {
            // Something went wrong with getReviews, simply skip the review-information in the response
            LOG.debug("Call to getListTweet failed: {}", listTweetResult.getStatusCode());
        } else {
            listTweet = listTweetResult.getBody();
        }

        return util.createOkResponse(new TweetAggregated(tweetResult.getBody(), retweet, listTweet));
    }
}
